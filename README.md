
# Newsoft Android Technical Challenge

 * Given the current skeleton of the project, implement a splash screen as well as a screen with a simple vertical scrolling list of employee cards.  
 * On the screen, we would like to immediately see a loading indicator in the form of a company logo with a "breathing" (pulsation) animation. Moreover, a list of scrollable elements, each of which displays the employee's image, full name, position, and hours logged. 
 * There should be an extended fab on the screen, which will change its state according to the scrolling of the list (scrolling down - collapsed state, scrolling up - extended state). This button is responsible for sorting the list by tracked hours and returning the list to the initial state.

A sample UI that you can create could be found below https://www.figma.com/file/JSkTjgWAoiugCHzUDIjSkR/newsoft-inside?node-id=0%3A1

## Technologies

### Kotlin Serialization
Serialization is the process of converting data used by an application to a format that can be transferred over a network or stored in a database or a file. In turn, deserialization is the opposite process of reading data from an external source and converting it into a runtime object. Together they are an essential part of most applications that exchange data with third parties.  
Documentation: https://kotlinlang.org/docs/serialization.html#example-json-serialization

### Ktor
Ktor is a framework to easily build connected applications – web applications, HTTP services, mobile and browser applications. Modern connected applications need to be asynchronous to provide the best experience to users, and Kotlin coroutines provide awesome facilities to do it in an easy and straightforward way. The goal of Ktor is to provide an end-to-end multiplatform framework for connected applications.  
Documentation: https://ktor.io/docs/getting-started-ktor-client.html

### Koin
If you want to work with a more Kotlin look-and-feel dependency injection framework? that is a good time to start work with Koin. This lightweight framework provides its dependency injection capabilities through a DSL.  
Documentation: https://ktor.io/docs/getting-started-ktor-client.html

### Coil
An image loading library for Android backed by Kotlin Coroutines. The main advantages of Coil are speed, lightness, ease of use, and modernity.  
Documentation: https://coil-kt.github.io/coil/

## Criteria
- You are not allowed to use libraries that provide an out-of-the-box screen saver
- The project must use libraries with versions specified in the application skeleton
- Adhere to the defined design (not a mandatory pixel-in-pixel approach, but mandatory preservation of proportions)
- Create an application yourself
- The task is considered completed after sending the finished application for review.