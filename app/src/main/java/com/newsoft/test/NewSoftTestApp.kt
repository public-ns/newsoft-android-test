package com.newsoft.test

import android.app.Application
import com.newsoft.test.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class NewSoftTestApp: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@NewSoftTestApp)
            modules(networkModule)
        }
    }
}