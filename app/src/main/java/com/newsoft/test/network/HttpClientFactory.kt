package com.newsoft.test.network

import io.ktor.client.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.features.*
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.serialization.json.Json

class HttpClientFactory {

    fun getHttpClient(): HttpClient {
        return HttpClient(OkHttp) {
            install(Logging) {
                logger = Logger.SIMPLE
                level = LogLevel.ALL
            }

            install(JsonFeature) {
                val json = Json {
                    ignoreUnknownKeys = true
                    coerceInputValues = true
                }
                serializer = KotlinxSerializer(json)
            }
            expectSuccess = false
            defaultRequest {
                host =
                    "54vmf9kwn7.execute-api.us-east-1.amazonaws.com"
                url {
                    protocol = URLProtocol.HTTPS
                }
                contentType(ContentType.Application.Json)
                header("X-api-key", "nfzazpxhbU6GHBNwXhO3A8UJYjrhvvKP4D6MTeUI")
            }

        }
    }
}