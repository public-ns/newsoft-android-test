package com.newsoft.test.network.service

interface EmployeeService {
    suspend fun fetchEmployee(): Any
}