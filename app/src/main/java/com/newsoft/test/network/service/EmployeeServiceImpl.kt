package com.newsoft.test.network.service

import io.ktor.client.*
import io.ktor.client.request.*

class EmployeeServiceImpl(
    private val httpClient: HttpClient
) : EmployeeService {
    override suspend fun fetchEmployee(): Any {
        return httpClient.get("/default/andriod-test-api")
    }
}


