package com.newsoft.test.di

import com.newsoft.test.network.HttpClientFactory
import com.newsoft.test.network.service.EmployeeService
import com.newsoft.test.network.service.EmployeeServiceImpl
import io.ktor.client.*
import org.koin.dsl.module

val networkModule = module {
    single<HttpClient> { HttpClientFactory().getHttpClient() }
    single<EmployeeService> { EmployeeServiceImpl(httpClient = get()) }
}